FROM registry.gitlab.com/alperkaangayretoglu/chess-website-frontend/base:latest as builder
WORKDIR /usr/local/app

COPY ./ ./

RUN npm run build


FROM node:18.16.0-alpine

EXPOSE 3000
WORKDIR /usr/local/app

COPY --from=builder /usr/local/app/public ./public

RUN mkdir .next

COPY --from=builder /usr/local/app/package.json /usr/local/app/package-lock.json ./
COPY --from=builder /usr/local/app/node_modules ./node_modules

COPY --from=builder /usr/local/app/.next/standalone ./
COPY --from=builder /usr/local/app/.next/static ./.next/static

CMD ["npm", "start"]
