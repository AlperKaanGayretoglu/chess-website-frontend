FROM node:18.16.0-alpine

WORKDIR /usr/local/app

COPY package.json package-lock.json ./

RUN npm install
